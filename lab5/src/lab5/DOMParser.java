/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 *
 * @author Porakit
 */
public class DOMParser {

  public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
        String filePath = "nation.xml";
        DocumentBuilderFactory factory
                = DocumentBuilderFactory.newInstance();
        DocumentBuilder parser
                = factory.newDocumentBuilder();
        Document xmlDoc = parser.parse(filePath);
        Element rootElement
                = xmlDoc.getDocumentElement();
        System.out.println("the root element name is " + rootElement.getNodeName());

    }
    
}
